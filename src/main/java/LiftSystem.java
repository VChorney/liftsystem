import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LiftSystem {

    public static Integer call(User user, Lift... lifts) {

        List<Lift> liftList = Stream.of(lifts).collect(Collectors.toList());
        Map<Integer, Integer> nearest = new HashMap<>();
        for (Lift lift : liftList) {
            if ((lift.getDirection().equals(user.getDirection()))) {
                if (((user.getFloor() > lift.getCurrentFloor()) && (user.getFloor() < lift.getGoalFloor())) || ((user.getFloor() < lift.getCurrentFloor()) && (user.getFloor() > lift.getGoalFloor()))) {
                    nearest.put(Math.abs(lift.getCurrentFloor() - user.getFloor()), lift.getId());
                }
            }
        }
        if (nearest.isEmpty()) {
            for (Lift lift : liftList) {
                if (lift.getDirection() == Direction.CALM) {
                    nearest.put(Math.abs(lift.getCurrentFloor() - user.getFloor()), lift.getId());
                }
            }
        }
        if (nearest.isEmpty()) {
            for (Lift lift : liftList) {
                nearest.put(Math.abs(lift.getGoalFloor() - user.getFloor()-lift.getCurrentFloor()), lift.getId());
            }
        }
        return nearest.get(nearest.keySet().toArray()[0]);
    }
}
