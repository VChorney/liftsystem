import java.util.concurrent.atomic.AtomicInteger;

public class Lift{
    private static final AtomicInteger idGenerator = new AtomicInteger(1);
    private int currentFloor;
    private int goalFloor;
    private int id;
    private Direction direction;

    public Lift(int currentFloor, int goalFloor) {
        this.id=idGenerator.getAndIncrement();
        this.currentFloor = currentFloor;
        this.goalFloor = goalFloor;
        if (currentFloor==goalFloor){
            this.direction= Direction.CALM;
        }
        if (currentFloor>goalFloor){
            this.direction= Direction.DOWN;
        }
        if (currentFloor<goalFloor){
            this.direction= Direction.UP;
        }

    }

    public int getId() {
        return id;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction =direction;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getGoalFloor() {
        return goalFloor;
    }

    public void setGoalFloor(int goalFloor) {
        this.goalFloor = goalFloor;
    }

}
