import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LiftSystemTest {
    @DataProvider(name = "Lift")
    public Object[][] parseLocaleData() {
        Lift[] lifts1 = {new Lift(1,11),new Lift(2,14),new Lift(10,25),new Lift(13,13),new Lift(12,5)};
        Lift[] lifts2 = {new Lift(1,11),new Lift(9,1),new Lift(8,12)};
        Lift[] lifts3 = {new Lift(1,12),new Lift(9,12),new Lift(3,3),new Lift(7,2)};

        return new Object[][]{
                {new User(12, User.Direction.UP), lifts1,3},
                {new User(10, User.Direction.DOWN),lifts2,6},
                {new User(8, User.Direction.UP),lifts3,9}
        };
    }

    @Test(dataProvider = "Lift")
    public void testCall(User user,Lift[] lifts,int expect) {
        final int actual = LiftSystem.call(user,lifts);
        assertEquals(actual,expect);
    }
}